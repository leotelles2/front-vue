// https://vuex.vuejs.org/en/actions.html
import axios from 'axios'
const url = 'http://ead-experience.herokuapp.com/';
// const url = 'http://localhost:8080/';
// import {Socket} from "phoenix"
const config = {
    headers: {
        'Content-Type': 'multipart/form-data',
        'Accept': '*/*',
        "Access-Control-Allow-Origin": "*"
    }
}
export default {
    CadastrarAluno: function({ commit }, userData) {
        return new Promise((resolve, reject) => {
            fetch(url + "autentificacao/cadastro/aluno/", params_multi_part(userData, 'POST'))
                .then((response) => {
                    console.log(response)
                    resolve(response.json())
                })
                .then((dataResponse) => {
                    console.log(dataResponse)
                })
                .catch((err) => {
                    console.log(err)
                    reject(err)
                })
        })
    },
    CadastrarProfessor: function({ commit }, userData) {
        return new Promise((resolve, reject) => {
            fetch(url + "autentificacao/cadastro/professor/", params_multi_part(userData, 'POST'))
                .then((response) => {
                    console.log(response)
                    resolve(response.json())
                })
                .then((dataResponse) => {
                    console.log(dataResponse)
                })
                .catch((err) => {
                    console.log(err)
                    reject(err)
                })
        })
    },
    DeletarMateria: function({ commit }, userData) {
        axios.post('/materia/del', userData)
            .then(function(response) {
                console.log(response);
            })
            .then(function(response) {
                console.log({ erro: response });
            });
    },
    EditarMateria: function({ comit }, userData) {
        console.log({ data: userData })
        fetch(url + "materia", params_multi_part(userData, 'PUT'))
            .then((response) => {
                console.log(response)
                return response.json()
            })
            .then((dataResponse) => {
                console.log(dataResponse)
            })
            .catch((err) => {
                console.log(err)
            })
    },
    CadastrarMateria: function({ commit }, userData) {
        console.log(userData)
        fetch(url + "materia", params_multi_part(userData, 'POST'))
            .then((response) => {
                console.log(response)
                return response.json()
            })
            .then((dataResponse) => {
                console.log(dataResponse)
            })
            .catch((err) => {
                console.log(err)
            })
    },
    CriarAula: function({ commit }, userData) {
        /*
        axios.post('/aula', userData)
        .then(function (response) {
            console.log(response);
        })
        .then(function (response) {
            console.log({erro: response});
        });  
        */
        return new Promise((resolve, reject) => {
            if (userData.idAula) {
                fetch(url + "aula", params_post(JSON.stringify(userData), 'application/json;charset=utf-8', 'PUT'))
                    .then((response) => {
                        return response.json()
                    })
                    .then((dataResponse) => {
                        resolve(dataResponse)
                    })
                    .catch((err) => {
                        console.log(err)
                        reject(err)
                    })
            } else {
                fetch(url + "aula", params_post(JSON.stringify(userData), 'application/json;charset=utf-8', 'POST'))
                    .then((response) => {
                        return response.json()
                    })
                    .then((dataResponse) => {
                        resolve(dataResponse)
                    })
                    .catch((err) => {
                        console.log(err)
                        reject(err)
                    })
            }
        })
    },
    AulaGravada: function({ commit }, userData) {
        return new Promise((resolve, reject) => {
            fetch(url + "aula/filtro", params_post(JSON.stringify(userData), 'application/json;charset=utf-8', 'POST'))
                .then((response) => {
                    return response.json()
                })
                .then((dataResponse) => {
                    console.log(dataResponse)
                    resolve(dataResponse)
                })
                .catch((err) => {
                    console.log(err)
                    reject(err)
                })
        })
    },
    Aulas: function({ commit }) {
        return new Promise((resolve, reject) => {
            /*
            axios.get('/aula')
                .then(function (response) {
                    resolve(response)
                })
                .catch(function (response) {
                    console.log({erro: response});
                    reject([])
                });
            */
            fetch(url + "aula", params_get())
                .then((response) => {
                    console.log(response)
                    return response.json()
                })
                .then((dataResponse) => {
                    console.log(dataResponse)
                    resolve(dataResponse)
                })
                .catch((err) => {
                    console.log(err)
                    reject(false)
                })
        })
    },
    Alunos: function({ commit }) {
        return new Promise((resolve, reject) => {
            fetch(url + "aluno", params_get())
                .then((response) => {
                    return response.json()
                })
                .then((dataResponse) => {
                    console.log(dataResponse)
                    resolve(dataResponse)
                })
                .catch((err) => {
                    console.log(err)
                    reject(false)
                })
        })
    },
    BuscarMaterias: function({ commit }) {
        return new Promise((resolve, reject) => {
            /*
                axios.get('/materia')
                    .then(function (response) {
                        resolve(response)
                    })
                    .catch(function (response) {
                        console.log({erro: response});
                        reject([])
                    });
            })
            */
            fetch(url + "materia", params_get())
                .then((response) => {
                    return response.json()
                })
                .then((dataResponse) => {
                    console.log(dataResponse)
                    resolve(dataResponse)
                })
                .catch((err) => {
                    console.log(err)
                    reject(false)
                })
        });
    },
    login: function({ commit }, userData) {
        return new Promise((resolve, reject) => {
            const dados = { login: userData.username, senha: userData.password };
            fetch(url + "autentificacao/login", params_post(JSON.stringify(dados), 'application/json;', 'POST'))
                .then((response) => {
                    console.log(response)
                    return response.json()
                })
                .then((dataResponse) => {
                    console.log(dataResponse)
                    console.log("data data data")
                    localStorage.setItem('token', 'TESTE123')
                    localStorage.setItem('user', JSON.stringify(dataResponse))
                    resolve(dataResponse)
                })
                .catch((err) => {
                    console.log(err)
                    reject(false)
                })
        })
    },
    AulaAlunoPersonalizada: function({ commit }, userData) {
        return new Promise((resolve, reject) => {
            fetch(url + "aula/aluno/" + userData.idAluno, params_get())
                .then((response) => {
                    console.log(response)
                    return response.json()
                })
                .then((dataResponse) => {
                    console.log(dataResponse)
                    resolve(dataResponse)
                })
                .catch((err) => {
                    console.log(err)
                    reject(false)
                })
        })
    },
    AulaAlunoTodas: function({ commit }, userData) {
        return new Promise((resolve, reject) => {
            fetch(url + "aula/aluno/todas/" + userData.idAluno, params_get())
                .then((response) => {
                    console.log(response)
                    return response.json()
                })
                .then((dataResponse) => {
                    console.log(dataResponse)
                    resolve(dataResponse)
                })
                .catch((err) => {
                    console.log(err)
                    reject(false)
                })
        })
    },
    AulaEncerrada: function({ commit }, userData) {
        return new Promise((resolve, reject) => {
            fetch(url + "aula/" + userData.id, params_post(null, 'application/json;', 'POST'))
                .then((response) => {
                    console.log(response)
                    return response.json()
                })
                .then((dataResponse) => {
                    console.log(dataResponse)
                    console.log("data data data")
                    resolve(dataResponse)
                })
                .catch((err) => {
                    console.log(err)
                    reject(false)
                })
        })
    },
    vincularMateria: function({ commit }, userData) {
        return new Promise((resolve, reject) => {
            fetch(url + "vincular/aluno-materia", params_post(JSON.stringify(userData), 'application/json;', 'POST'))
                .then((response) => {
                    console.log(response)
                    return response.json()
                })
                .then((dataResponse) => {
                    console.log(dataResponse)
                    console.log("data data data")
                    resolve(dataResponse)
                })
                .catch((err) => {
                    console.log(err)
                    reject(false)
                })
        })
    },
    logout({ commit }) {
        return new Promise((resolve, reject) => {
            commit('logout')
            localStorage.removeItem('token')
            localStorage.removeItem('user')
            resolve()
        })
    }
}

function params_post(body, content_type, meth) {
    let value = {
        method: meth,
        headers: {
            'Accept': '*/*',
            'Content-Type': content_type
        },
        mode: 'cors',
        body: body
    }
    return value
}

function params_multi_part(body, meth) {
    let value = {
        method: meth,
        headers: {
            'Accept': '*/*',
            'Accept-Encoding': 'multipart/form-data'
        },
        mode: 'cors',
        body: body
    }
    return value
}

function params_get() {
    let value = {
        method: 'GET',
        headers: {
            'Accept': '*/*',
            'Content-Type': "application/json;"
        },
        mode: 'cors'
    }
    return value
}
