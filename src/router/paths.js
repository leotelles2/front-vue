/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
import store from '../store'
export default [{
        path: '*',
        meta: {
            name: '',
            requiresAuth: true
        },
        redirect: {
            path: '/'
        }
    },
    {
        path: '/',
        meta: {
            name: '',
            requiresAuth: false
        },
        component: () =>
            import (`@/views/LoginHome.vue`),
        beforeEnter: (to, from, next) => {
            if (store.getters.authorized) {
                next('/ead')
            } else {
                next()
            }
        },
        children: [{
            path: '',
            component: () =>
                import (`@/components/LoginForm.vue`)
        }]
    },
    {
        path: '/ead',
        meta: {
            name: 'Dashboard View',
            requiresAuth: true
        },
        component: () =>
            import (`@/views/DashboardView.vue`),
        children: [{
                path: '',
                name: 'Dashboard',
                component: () =>
                    import (`@/components/DashViews/Dashboard.vue`)
            },
            {
                path: 'aulas',
                meta: {
                    name: 'aulas',
                    requiresAuth: true
                },
                component: () =>
                    import (`@/components/DashViews/Aulas.vue`)
            },
            {
                path: 'solicitar',
                meta: {
                    name: 'solicitar',
                    requiresAuth: true
                },
                component: () =>
                    import (`@/components/DashViews/Solicitar.vue`)
            },
            {
                path: 'aulaonline',
                meta: {
                    name: 'aulaonline',
                    requiresAuth: true
                },
                component: () =>
                    import (`@/components/DashViews/AulaOnline.vue`)
            },
            {
                path: 'gerenciarmateria',
                meta: {
                    name: 'gerenciarmateria',
                    requiresAuth: true
                },
                component: () =>
                    import (`@/components/DashViews/GerenciarMateria.vue`)
            },
            {
                path: 'typography',
                meta: {
                    name: 'Typography',
                    requiresAuth: true
                },
                component: () =>
                    import (`@/components/DashViews/Typography.vue`)
            },
            {
                path: 'agendaaulas',
                meta: {
                    name: 'agendaaulas',
                    requiresAuth: true
                },
                component: () =>
                    import (`@/components/DashViews/AgendaAulas.vue`)
            },
            {
                path: 'iniciaraula',
                meta: {
                    name: 'iniciaraula',
                    requiresAuth: true
                },
                component: () =>
                    import (`@/components/DashViews/IniciarAula.vue`)
            },
            {
                path: 'GerenciarAluno',
                meta: {
                    name: 'GerenciarAluno',
                    requiresAuth: true
                },
                component: () =>
                    import (`@/components/DashViews/GerenciarAluno.vue`)
            }
        ]
    }
]
